package main

import (
	"log"
	"net/http"
)

func main() {
	log.Println("serving files under /etc/config/webserver")
	fs := http.FileServer(http.Dir("/etc/config/webserver"))
	http.Handle("/", fs)

	log.Println("Listening on :8080...")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
